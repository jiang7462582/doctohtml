package models

import akka.actor.{Props, ActorSystem, Actor}
import akka.util.Timeout
import models.ConversionActor.UploadImage
import play.api.Logger
import play.api.libs.json._
import utils.{Md5, QiNiu, Doc2Html}
import java.util.regex.Matcher
import java.util.regex.Pattern
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.language.postfixOps

import scala.util.{Failure, Success, Try}

import models.ConversionActor.{Translate,UploadImage}



/**
  * Created by jiang on 15/12/7.
  */

case class Task(
               var name:Option[String] = None,
               var md5:Option[String] = None,
               var is_exist:Option[Int] = Option(-1)
               )
case class AmqData(
                    var md5:Option[String] =None,
                    var doctype:Option[String] = None
                  )


trait ConversionJsonTrait {
  implicit val TaskFormat = Json.format[Task]
  implicit val AmqDataFormat = Json.format[AmqData]
}


// 上传qiniu,保存文件到HTMLDir 文件夹
import java.io._
object Conversion {

  implicit lazy val system = ActorSystem()
  implicit val timeout = Timeout(300 seconds)
  implicit lazy val actor = system.actorOf(Props[ConversionActor])

  def translate(fromPath:String,toPath:String) = {
    val start = System.currentTimeMillis()
    Logger.info("开始转换:"+ fromPath + ":" +start)
    val tmpPath = "/tmp/"+toPath  //临时存放文件
    new Doc2Html().apply(fromPath,tmpPath)

    // 判断文件是否存在 输出状态 同时清除格式
    val state = Try(scala.io.Source.fromFile(tmpPath)) match  {
      case Success(source) => {
        val lines = try source.getLines mkString "\n"  finally source.close()
        val html = Conversion.clearFormat(lines,"doc/")
        val content = Conversion.replaceHtmlTag(html, "jiang")
        // 写入新文件
        var read:Int = 0
        val bytes:Array[Byte] =new  Array(1024)
        var out:OutputStream = null
        try{
          out = new FileOutputStream(toPath)
          val in_withcode:InputStream = new ByteArrayInputStream(content.getBytes("UTF-8"))
          while ( {read = in_withcode.read(bytes); read != -1}){
            out.write(bytes, 0, read)
          }
          out.flush()
          out.close()
        }catch {
          case e:Exception => println(e.getMessage)
        }

        Option(1)
      }
      case _ => println("NO"); Option(-1)
    }

    val end = System.currentTimeMillis()

    Logger.info("转换完成:"+end)
    Logger.info((start-end).toString)
    val usetime = (end-start)/1000


    Json.toJson(state)
  }


  def clearFormat(htmlStr:String,docImgPath:String) ={
    val bodyReg:String = "<BODY .*</BODY>"

    val bodyPattern:Pattern = Pattern.compile(bodyReg)
    val bodyMatcher:Matcher = bodyPattern.matcher(htmlStr)
    var htmlStrTemp:String = htmlStr
    if (bodyMatcher.find()) {
      // 获取BODY内容，并转化BODY标签为DIV
      htmlStrTemp = bodyMatcher.group().replaceFirst("<BODY", "<DIV")
        .replaceAll("</BODY>", "</DIV>")
    }
    // 调整图片地址
    //    htmlStrTemp = htmlStrTemp.replaceAll("<img src=\"", "<IMG SRC=\"" + docImgPath + "/")
    // 把<P></P>转换成</div></div>保留样式
    // content = content.replaceAll("(<P)([^>]*>.*?)(<\\/P>)",
    // "<div$2</div>");
    // 把<P></P>转换成</div></div>并删除样式
    htmlStrTemp = htmlStrTemp.replaceAll("(<P)([^>]*)(>.*?)(<\\/P>)", "<p$3</p>")
    // 删除不需要的标签
    htmlStrTemp = htmlStrTemp.replaceAll(
      "<[/]?(font|FONT|span|SPAN|xml|XML|del|DEL|ins|INS|meta|META|[ovwxpOVWXP]:\\w+)[^>]*?>", "")
    // 删除不需要的属性
    htmlStrTemp = htmlStrTemp.replaceAll("<([^>]*)(?:lang|LANG|class|CLASS|style|STYLE|size|SIZE|face|FACE|[ovwxpOVWXP]:\\w+)=(?:'[^']*'|\"\"[^\"\"]*\"\"|[^>]+)([^>]*)>",
      "<$1$2>")
    htmlStrTemp

  }

  def replaceHtmlTag(str:String, out_name:String)={
    val regxpForTag:String = "(<\\s*img\\s+[^>]*\\s*)(src=\"[^\"]+\")(\\s*[^>]*\\s*>)"
    val regxpForTagAttrib:String = "src=\"([^\"]+)\""
    val patternForTag:Pattern = Pattern.compile(regxpForTag, Pattern. CASE_INSENSITIVE )
    val patternForAttrib:Pattern = Pattern.compile(regxpForTagAttrib, Pattern. CASE_INSENSITIVE)
    val matcherForTag:Matcher = patternForTag.matcher(str)
    val sb:StringBuffer = new StringBuffer()
    var result:Boolean = matcherForTag.find()
    while(result){
      val  sbreplace:StringBuffer = new StringBuffer()
      val matcherForAttrib:Matcher = patternForAttrib.matcher(matcherForTag.group(2))
      if(matcherForAttrib.find()){
        val image_ary = matcherForAttrib.group(1).split(",")
        val image_type = image_ary(0)
        val image_content = image_ary(1)
        var imagetype = ""
        if(image_type.indexOf("data:image/png")!= -1){
          imagetype = ".png"
        }else{
          if(image_type.indexOf("data:image/jpeg")!= -1){
            imagetype = ".jpg"
          }
        }

        val md5 = Md5.apply(matcherForAttrib.group(1))
        import org.apache.commons.codec.binary._
        val bytes = Base64.decodeBase64(image_content)
        val remote_path = QiNiu.getRoot+"/"+out_name+"/"+md5+imagetype
        matcherForAttrib.appendReplacement(sbreplace, matcherForTag.group(1)+"src=\"" + remote_path + "\""+matcherForTag.group(3))

        val name:String = out_name + "/" + md5 + imagetype
        actor ! UploadImage(bytes,name)

         //上传
//        new Thread (new Runnable() {
//          @Override
//          def run() {
//            QiNiu.uploadByte(bytes, name)
//          }
//        }).start()

      }
      matcherForTag.appendReplacement(sb, sbreplace.toString)
      result = matcherForTag.find()
    }
    matcherForTag.appendTail(sb)
    sb.toString
  }




}










object ConversionActor{
  case class Translate(fromPath:String,toPath:String)
  case class UploadImage(data:Array[Byte],name:String)
}

import models.ConversionActor.{Translate,UploadImage}

class ConversionActor extends Actor {
  def receive:Receive = {
    case Translate(fromPath:String,toPath:String) => {
      sender ! Conversion.translate(fromPath,toPath)
    }
    case UploadImage(data:Array[Byte],name:String) =>{
      sender ! QiNiu.uploadByte(data,name)
    }
  }
}