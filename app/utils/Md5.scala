package utils

/**
  * Created by jiang on 15/11/26.
  */

import java.io.FileInputStream
import java.security.MessageDigest
object Md5 {
  def apply(bytes: String):String= {
    val md5 = MessageDigest.getInstance("MD5")
    md5.reset()
    md5.update(bytes.toCharArray.map(_.asInstanceOf[Byte]))
    md5.digest().map(0xFF & _).map { "%02x".format(_) }.foldLeft(""){_ + _}
  }
  def getMd5(fileName:String):String={
    val fileStream = new FileInputStream(fileName)
    val buffer = new Array[Byte](4096)
    var len = fileStream.read(buffer)
    val digest = MessageDigest.getInstance("MD5")
    while(len != -1) {
      digest.update(buffer,0,len)
      len = fileStream.read(buffer)
    }
    digest.digest().map("%02x".format(_)).mkString.toLowerCase
  }
}