package utils

import javax.jms.Connection
import javax.jms.Destination
import javax.jms.JMSException
import javax.jms.Message
import javax.jms.MessageConsumer
import javax.jms.MessageListener
import javax.jms.MessageProducer
import javax.jms.Session
import javax.jms.TextMessage

import models.{ConversionActor, ConversionJsonTrait, AmqData}
import org.apache.activemq.ActiveMQConnectionFactory
import org.apache.activemq.pool.PooledConnectionFactory

import play.api.Play
import play.api.libs.json._

import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.Await
import scala.concurrent.duration._
import akka.actor.{Props, ActorSystem}

import scala.language.postfixOps

/**
  * Created by jiang on 15/12/10.
  */




object converMQ extends ConversionJsonTrait{
  val activeMQServerIPAddr = Play.current.configuration.getString("activemq.server.ip").getOrElse("")
  val activeMQServerUser = Play.current.configuration.getString("activemq.server.user").getOrElse("")
  val activeMQServerPasswd = Play.current.configuration.getString("activemq.server.passwd").getOrElse("")
  val factory: ActiveMQConnectionFactory = new ActiveMQConnectionFactory(activeMQServerIPAddr)
  val pooledMaxConnectionSize: Int = 10
  val pooledFactory: PooledConnectionFactory = new PooledConnectionFactory(factory)
  pooledFactory.setMaxConnections(pooledMaxConnectionSize)
  val queueName = "doc2html"

  implicit val timeout = Timeout(10 seconds)
  implicit lazy val system = ActorSystem()
  implicit lazy val actor = system.actorOf(Props[ConversionActor])

  def send(textMsg: String) = {
    val connection = pooledFactory.createConnection(activeMQServerUser, activeMQServerPasswd)
    val session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE)
    val destination = session.createQueue(queueName)
    val producer = session.createProducer(destination)
    producer.send(session.createTextMessage(textMsg))
    session.close()
  }

  def recv: Session = {
    val cf = new ActiveMQConnectionFactory(activeMQServerIPAddr)
    cf.setCopyMessageOnSend(false)
    val connection = cf.createConnection()
    connection.start()
    val session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE)
    val destination = session.createQueue(queueName)
    val consumer = session.createConsumer(destination)
    val listener = new MessageListener {
      def onMessage(message: Message) {
        message match {
          case text: TextMessage => {
            // consumer message via ws
            val textContentComp = text.getText
            // decompress the text msg received
            // ToDo
            val dataJson = Json.parse(textContentComp)
            dataJson.validate[AmqData] match {
              case s: JsSuccess[AmqData] => {
                val amqdata = s.get
                val md5 = amqdata.md5.getOrElse("0")
                val doctype = amqdata.doctype.getOrElse("doc")
                val filepath = "/usr/local/data/"
                val fromPath = filepath + md5.toLowerCase  +"."+ doctype  //服务器上已保存待转换的文件
                val toPath = filepath + "result/" + md5.toLowerCase + ".html"

                val future = actor ?  ConversionActor.Translate(fromPath,toPath)
                Await.result(future, timeout.duration).asInstanceOf[JsValue]
              }
              case e: JsError => {
                // error handling flow
              }
            }




          }
          case _ => {
            throw new Exception("Unhandled message type: " + message.getClass.getSimpleName)
          }
        }
      }
    }
    consumer.setMessageListener(listener)
    session

  }

}
