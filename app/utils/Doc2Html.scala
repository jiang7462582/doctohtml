package utils

/**
 * Created by jiang on 15-10-15.
 */

import java.io.File
import play.api._
import org.artofsolving.jodconverter.OfficeDocumentConverter
import org.artofsolving.jodconverter.office.DefaultOfficeManagerConfiguration
import org.artofsolving.jodconverter.office.OfficeManager

import javax.inject._

@Singleton
class Doc2Html {
  var officeManager: OfficeManager = null

  def apply(inpath:String,outpath:String)={
    try {

      // 注意部署服务器后需要更改配置地址

      officeManager = new DefaultOfficeManagerConfiguration()
        .setOfficeHome(new File("/Applications/LibreOffice.app/Contents"))
        .buildOfficeManager()
      officeManager.start()
      val converter: OfficeDocumentConverter = new OfficeDocumentConverter(officeManager)
      converter.convert(new File(inpath), new File(outpath))
    }catch{
      case e:Exception=>println(e)
    }finally {
      if(officeManager!=null){
        officeManager.stop()
      }

    }
  }

}




