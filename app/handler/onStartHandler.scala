package handler

/**
  * Created by jiang on 15/12/9.
  */

import com.google.inject.AbstractModule
import com.google.inject.name.Names
import play.api.cache._
import javax.inject.Inject
import com.google.inject.ImplementedBy

import javax.jms.Session
import utils.converMQ

import scala.collection.mutable.ListBuffer


/**
  * Created by jiang on 15/12/9.
  */


class onStartHandler extends AbstractModule{
  def configure()={
    bind(classOf[OnStartTrait])
      .annotatedWith(Names.named("Loginfo"))
      .to(classOf[OnStart]).asEagerSingleton()
  }

}



@ImplementedBy(classOf[OnStart])
trait OnStartTrait{

}

class  OnStart @Inject() (cache: CacheApi) extends OnStartTrait{
  // do your code here

  play.api.Logger.info("doc2Html Start !")

//  private lazy val consumers = new ListBuffer[Session]()
//  consumers += converMQ.recv





}