package controllers


import java.awt.Image

import models._
import play.api.libs.ws.WS

import play.api.mvc._
import play.api.libs.json._

import utils.{Md5, QiNiu,converMQ}




import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import play.api.Play.current
import scala.concurrent.ExecutionContext.Implicits.global

import scala.language.postfixOps
import akka.actor.{Props, ActorSystem}
import models.ConversionActor.Translate

import scala.util.{Success, Try, Failure}
import play.api.cache._
import javax.inject._

class Application @Inject()(cache:CacheApi)  extends Controller with ConversionJsonTrait{

  implicit lazy val system = ActorSystem()
  implicit val timeout = Timeout(300 seconds)
  implicit lazy val actor = system.actorOf(Props[ConversionActor])


  def index = Action {

    import javax.jms.Session
    import utils.converMQ

    import scala.collection.mutable.ListBuffer
//    val consumers = new ListBuffer[Session]()
//    consumers += converMQ.recv
    Ok(views.html.index("Your new application is ready."))
  }

  def file = Action {
    Ok(views.html.fileUpload())
  }

  // 上传文件
  def upload = Action(parse.multipartFormData){ request =>
    request.body.file("file").map { file =>
      import java.io.File
      val filename = file.filename
      val md5= Md5.getMd5(file.ref.file.toString)
      val toPath="/usr/local/data/"+md5.toLowerCase+"."+filename.toString.split("\\.").last
      // 判断转换后的文件是否存在
      val htmlFile = new File("/usr/local/data/result/"+md5.toLowerCase +".html")
      val is_existOpt = if(htmlFile.exists()) Option(1) else Option(-1)
      file.ref.moveTo(new File(toPath)) // 保存文件
      val ret = Task(Option(filename),Option(md5.toString.toLowerCase),is_existOpt)
      Ok(Json.toJson(ret))

    }.getOrElse {
      Redirect(routes.Application.index).flashing(
        "error" -> "Missing file"
      )
    }
  }

  def conver(md5:String,doctype:String) = Action{


    val filepath = "/usr/local/data/"
    val fromPath = filepath + md5.toLowerCase  +"."+ doctype  //服务器上已保存待转换的文件
    val toPath = filepath + "result/" + md5.toLowerCase + ".html"


    // 检查文件是否存在
       val ret = Try(scala.io.Source.fromFile(fromPath)) match {
          case Failure(source) => Json.toJson(Option(-1))
          case Success(source) =>{
            // Test 消息队列模式
            val amqdata = AmqData(Option(md5),Option(doctype))
            converMQ.send(Json.toJson(amqdata).toString)



//           val future = actor ?  ConversionActor.Translate(fromPath,toPath)
//            Await.result(future, timeout.duration).asInstanceOf[JsValue]
            Json.toJson(Option(1))




          }  //发送到Akka  或者消息队列
        }


//    Status(ret.toString().toInt)
    Ok(Json.obj()+("Status"->ret))

  }

  def getHtml(md5:String) = Action{
    val filepath = "/usr/local/data/result/"
    val path =filepath + md5.toLowerCase +".html"
    val lines = Try(scala.io.Source.fromFile(path)) match  {
      case Success(source) => try source.getLines mkString "\n"  finally source.close()
      case _ =>"文件不存在,请重新上传转换操作!"
    }

    Ok(lines).as(HTML)

  }


}

