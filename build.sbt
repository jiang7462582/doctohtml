name := """docToHtml"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  specs2 % Test,
"commons-codec" % "commons-codec" % "1.9",
"org.xerial.snappy" % "snappy-java" % "1.1.0",
"org.apache.activemq" % "activemq-all" % "5.9.0",
"org.apache.activemq" % "activemq-pool" % "5.9.0"
)




libraryDependencies += "com.qiniu" % "qiniu-java-sdk" % "7.0.7"

libraryDependencies += "com.typesafe.akka" % "akka-actor_2.11" % "2.3.14"

libraryDependencies += "com.aliyun.oss" % "aliyun-sdk-oss" % "2.0.7"


resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"



// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator
